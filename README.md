# Automatic Sakai Homework Opener

A short batch script to allow for easier grading of Sakai submissions. 
It will automatically open student submissions so you don't have to 
deal with Sakai's interface. Only runs on Windows.

Usage:  
- Download the hwOpener.bat file

- Download all student submissions from the Sakai assignment (see Instructions file if you need help), extract the folder, then place the extracted student submission folder in the same directory as the batch file. (i.e. The batch file should be in the same directory as your "Homework 01" folder, not inside that folder)
        
- If you enter a nonexistent assignment, the script will just exit.
        
- I recommend leaving the script in one directory, then adding new assignment folders as you get more of them.
        
- The script will allow you to skip forward to whichever submission number you want, provided that you've entered a valid number.
        
Notes:  
- The script will work for any assignment and an arbitrary number of students/submissions.
        
- Keep in mind that a student's number in the class roster might not match their submission number in the assignment folder because of missing submissions, so you may need to use trial and error to find where the submission you're looking for is in the order.
        
- The script will use whatever your system's default program is for opening a given file type.
        
- Sorry in advance if the code doesn't make any sense; batch is a weird language and I don't know it super well. Everything should work though.

v. 1.0  

Writted by Jacob Mazur, jmazur2@nd.edu

Feel free to reach out if you have any questions
