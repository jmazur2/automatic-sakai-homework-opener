To download student submissions from Sakai:

- Navigate to the Assignments tab for the class
- Select "Grade" or "View Submissions" under the assignment you want, whichever shows up
- Click on "Download All" at the top of the screen
- Select the "Student submission attachment(s)" option
- Click "Download"
- Extract the .zip file to the same directory as hwOpener.bat
