@ECHO OFF

REM Automatic Sakai Homework Opener
REM This script will open all the files students submitted to Sakai for a given assignment
REM Place the script in the same directory as your extracted submissions folders.
REM (i.e. in the same directory as your "Homework 01" folder, not inside that folder)

REM Notes:

REM The script will work for any assignment and an arbitrary number of students/submissions.

REM Keep in mind that a student's number in the class roster might not match their submission number 
REM in the assignment folder because of missing submissions, so you may need to use trial and error
REM to find where the submission you're looking for is in the order.

REM The script will use whatever your system's default program is for opening a given file type.

REM Sorry in advance if the code doesn't make any sense; batch is a weird language
REM and I don't know it super well. Everything should work though.

REM v. 1.0 
REM Updated 2/1/2020
REM Written by Jacob Mazur, jmazur2@nd.edu
REM Feel free to reach out if you have any questions



REM Script begins here

echo Automatic Sakai Homework Opener
echo.
echo Usage: Download all student submissions from the Sakai assignment, extract the folder,
echo then place this batch file in the same directory as the extracted student submission folder.
echo.
set /p name="Enter name of assignment folder (e.g. Homework 01): "
set currPath=%CD%\%name%


REM Counts up number of submissions
set /a submissionCount=0
for /D %%Q IN ("%currPath%"\*) DO set /a submissionCount += 1


set studentNum=0
set /p userIn="Begin opening files at submission #(1-%submissionCount%): "
set /a studentNum=%userIn%*1


REM Tests if user entered a valid starting number for the assignment.
if not %studentNum% == %userIn% (
	echo Error -- you must enter a number.
	pause
	exit
)
if %studentNum% GTR %submissionCount% (
	echo Error -- your beginning submission number is out of bounds. There are only %submissionCount% submisssions.
	pause
	exit
)
if %studentNum% LSS 0 (
	echo Error -- your beginning submission number is out of bounds. Enter one between 1 and %submissionCount%.
	pause
	exit
)


set /a it=1

for /D %%G IN ("%currPath%"\*) DO (
	echo.
	echo Student Name: %%~nG
	CALL :openFolder1 "%%G"
)

echo Reached end of student submissions.
pause
exit

REM Main script ends here


REM Functions:

REM Skips folder if studentNum not reached, otherwise opens student's folder
:openFolder1
if %it% LSS %studentNum% (
	echo --- Skipping Student ---
	set /a it+=1
) else (
	CALL :openFolder2 "%~1"
)
goto:EOF


REM Opens folder inside of student folder 
:openFolder2
for /D %%I IN ("%~1"\*) DO CALL :openFile "%%I"
goto:EOF


REM Function to open files inside of submitted attachments folder using your default program for the file type
:openFile
for %%J IN ("%~1"\*) DO (
	start "" "%%J"
	)
CALL :testConditions
goto:EOF


REM Function to test exit conditions
:testConditions
set /p input="Hit enter to open next file, q to quit: "
if "%input%" == "q" (
	exit
)
goto:EOF
